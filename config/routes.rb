Rails.application.routes.draw do
  resources :publications
  put 'delete-image/:publication_id', to: 'publications#delete_image', as: :delete_image 
  root 'pages#home'
end
