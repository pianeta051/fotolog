class PagesController < ApplicationController
  def home
    if Publication.count > 1
      @latest_publications = Publication
        .all
        .order(created_at: :desc)
        .where
        .not(id: Publication.last.id)
        .limit(3)
    end
  end
end
